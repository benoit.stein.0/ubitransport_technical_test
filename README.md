# Ubitransport Technical Test
Technical Test Symfony  API Platform For Ubitransport

## Installation

1. Clone the project

2. Go into your project and run 
```sh 
composer install
```

3. Copy the `.env.example` in a `.env.local` to configure your database.

4. Lauch your local server
```sh
symfony serve -d --no-tls
```

5. Run the command to install your database.
```sh
php bin/console doctrine:database:create
```

6. Initialize your database with the current migration file
```sh
php bin/console doctrine:migrations:migrate
```

## Usage
Go to your local server and add /api to your address. You will have access to all the functionnalities. 

### Student

#### Add a student 
1. Click on the endpoint "POST" `/api/students`, then `Try It Out` 
2. Insert informations like this 
```sh
{
  "firstname": "benoît",
  "lastname": "stein",
  "birthday": "1991-08-14T12:49:45.453Z"
}
```
3. Execute.

#### List of all students
1. Click on the endpoint "GET" `/api/students`, then `Try It Out` 
2. Give the page you want.
3. Execute. 

#### Informations for one student 
1. You need to have the `id` corresponding to the current student.
2. Click on the endpoint "GET" `/api/students/{id}`, then `Try It Out` 
3. Give the `id` of the student 
4. Execute. 

#### Update a student
1. You need to have the `id` corresponding to the current student.
2. Click on the endpoint "PATCH" `/api/students/{id}`, then `Try It Out` 
3. Give the `id` of the student and Update the request body.
4. Execute. 

#### Delete a student
1. You need to have the `id` corresponding to the current student.
2. Click on the endpoint "DELETE" `/api/students/{id}`, then `Try It Out` 
3. Give the `id` of the student
4. Execute. 

### Marks

#### Add a Mark for a Student
1. You need to have the `@id` corresponding to the current student (exemple : `api/students/1`) (You can use the endpoint about `Informations for one studen` to have it)
2. Click on the endpoint "POST" `/api/marks`, then `Try It Out`
3. Insert informations like this 
```sh
{
  "value": 12,
  "subject": "français",
  "student": "api/students/1"
}
```
4. Excecute.

### Average for a Student
1. You need to have the `id` corresponding to the current student.
2. Click on the endpoint "GET" `/api/average_students/{id}`, then `Try It Out`
3. Give the `id` of the student.
4. Execute.

### Average Classroom (only with the API interface)
1. Click on the endpoint "GET" `/average_classroom`.

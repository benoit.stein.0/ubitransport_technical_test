<?php

declare(strict_types=1);

namespace App\Repository\AverageStudent;

use App\Entity\AverageStudent;

interface AverageStudentDataInterface
{
    /**
     * @return array<int, AverageStudent>
     */
    public function getAverageStudent(): array;
}

<?php

declare(strict_types=1);

namespace App\Repository\AverageStudent;

use App\Entity\AverageStudent;
use App\Repository\MarkRepository;
use App\Repository\StudentRepository;

final class AverageStudentDataRepository implements AverageStudentDataInterface
{
    private MarkRepository $markRepository;

    public function __construct(MarkRepository $markRepository)
    {
        $this->markRepository = $markRepository;
    }

    /**
     * @return array<int, AverageStudent>
     */
    public function getAverageStudent(): array
    {
        $marks = $this->markRepository->findAll();

        foreach ($marks as $mark) {
            $students[$mark->getStudent()->getId()]['id'] = $mark->getStudent()->getId();
            $students[$mark->getStudent()->getId()]['lastname'] = $mark->getStudent()->getLastname();
            $students[$mark->getStudent()->getId()]['firstname'] = $mark->getStudent()->getFirstname();
            $students[$mark->getStudent()->getId()]['marks'][] = $mark;
        }

        foreach ($students as $student) {
            $countMatters = count($student['marks']);

            $sumMarks = 0;
            foreach ($student['marks'] as $mark) {
                $sumMarks += $mark->getValue();
            }
            $averageStudent = new AverageStudent($student['id'], $student['lastname'], $student['firstname'], $sumMarks / $countMatters);

            $averagesStudents[$student['id']] = $averageStudent;
        }

        return $averagesStudents ?? [];
    }
}

<?php

namespace App\Repository;

use App\Entity\AverageStudent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AverageStudent|null find($id, $lockMode = null, $lockVersion = null)
 * @method AverageStudent|null findOneBy(array $criteria, array $orderBy = null)
 * @method AverageStudent[]    findAll()
 * @method AverageStudent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AverageStudentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AverageStudent::class);
    }

    // /**
    //  * @return AverageStudent[] Returns an array of AverageStudent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AverageStudent
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

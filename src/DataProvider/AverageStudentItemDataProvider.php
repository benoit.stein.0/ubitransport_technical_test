<?php

declare(strict_types=1);

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use ApiPlatform\Core\Exception\InvalidIdentifierException;
use App\Entity\AverageStudent;
use App\Repository\AverageStudent\AverageStudentDataInterface;

final class AverageStudentItemDataProvider implements ItemDataProviderInterface, RestrictedDataProviderInterface
{
    private AverageStudentDataInterface $repository;

    public function __construct(AverageStudentDataInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $resourceClass
     * @param string|null $operationName
     * @param array<string, mixed> $context
     * @return bool
     */
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return AverageStudent::class === $resourceClass;
    }

    /**
     * @param string $resourceClass
     * @param $id
     * @param string|null $operationName
     * @param array<string, mixed> $context
     *
     * @return AverageStudent|null
     * @throws InvalidIdentifierException
     * @phpstan-ignore-next-line
     */
    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = []): ?AverageStudent
    {
        if (!is_int($id)) {
            throw new InvalidIdentifierException('Invalid id key type.');
        }

        try {
            $averagesStudent = $this->repository->getAverageStudent();
        } catch (\Exception $e) {
            throw new \RuntimeException(sprintf('Unable to retrieve average student: %s', $e->getMessage()));
        }

        return $averagesStudent[$id] ?? null;
    }
}

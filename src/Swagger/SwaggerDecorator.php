<?php

namespace App\Swagger;

use App\Repository\MarkRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class SwaggerDecorator
 * @package App\Swagger
 */
final class SwaggerDecorator implements NormalizerInterface
{
    private NormalizerInterface $decorated;
    private MarkRepository $markRepository;

    public function __construct(NormalizerInterface $decorated, MarkRepository $markRepository)
    {
        $this->decorated = $decorated;
        $this->markRepository = $markRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        $sumMarks = 0;
        $marks = $this->markRepository->findAll();

        foreach ($marks as $mark) {
            $sumMarks += $mark->getValue();
        }

        $docs['paths']['/average_classroom']['get'] = [
            'summary' => 'Retrieves the average of the classroom',
            'tags' => ['Average Classroom'],
            'responses' => [
                Response::HTTP_OK => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'average' => [
                                        'type' => 'float|string',
                                        'example' => !empty($marks) && isset($marks) ? $sumMarks / count($marks) : 'No marks at the moment ',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
        return $docs;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }
}
